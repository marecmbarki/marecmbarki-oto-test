$(document).ready(function() {
  $("#headerMobile").load("header/header-mobile.html");
  $("#headerDesktop").load("header/header-desktop.html");
  $("#presentationFirst").load("carousel/presentation.html");
  $("#presentationSecond").load("carousel/presentation.html");
  $("#presentationThird").load("carousel/presentation.html");
  $("#footer").load("footer/index.html");
});