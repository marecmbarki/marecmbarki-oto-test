# marecmbarki-oto-test
***
Model integration test
## Installation
***
The project needs to run a server to avoid CORS errors related to the jQuery ajax load method.
```
$ git clone "https://gitlab.com/marecmbarki/marecmbarki-oto-test.git"
$ cd .\marecmbarki-oto-test\
-Download the google chrome's plug-in named "Web server for chrome" (https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb).
-Click on the launch application button.
-Turn on the web server and then choose the folder "marecmbarki-oto-test".
-Choose an Enter Port or take the default one and go to the following url with the browser of your choice : "YourEnterPort/view/".
```

### Side Information
> I used css instead of sass for the style because the implementation of a sass compilation tool would have made the implementation of the project more complex.
> I used jQuery to load html snippets to make reusable components.

